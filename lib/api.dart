import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Post {
  final int userId;
  final int id;
  final String title;
  final String body;

  Post({this.userId, this.id, this.title, this.body});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      userId: json['userId'],
      id: json['id'],
      title: json['Title'],
      body: json['Body'],
    );
  }
}

Future<List<Post>> fetchPosts() async {
  final response = await http.get('http://192.168.100.196/api/posts');

  if (response.statusCode == 200) {
    List<Post> posts = new List();
    List<dynamic> data = json.decode(response.body)['data'];

    data.forEach((dynamic postJson) {
        posts.add(new Post.fromJson(postJson));
    });

    return posts;
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}